import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("io.spring.dependency-management") version "1.0.14.RELEASE"
    id("org.springframework.boot") version "2.7.5" apply false

    id("maven-publish")
    id("java-library")

    kotlin("jvm") version "1.7.10"
    kotlin("plugin.spring") version "1.7.10"

}

group = "com.cerocss"

java {
    sourceCompatibility = JavaVersion.VERSION_17
    withSourcesJar()
    withJavadocJar()
}

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")

    implementation("dev.kord:kord-core:0.8.0-M16")
    testImplementation("io.mockk:mockk:1.13.1")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.6.4")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "17"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

dependencyManagement {
    imports {
        mavenBom(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES)
    }
}

tasks.jar {
    manifest {
        attributes(
            mapOf(
                "Implementation-Title" to project.name,
                "Implementation-Version" to project.version
            )
        )
    }
}

publishing {
    publications {
        create<MavenPublication>("library") {
            pom {
                name.set("springkord")
                description.set("Run Kord Bot using spring boot")
                url.set("https://cerocss.gitlab.io/springkord")
                licenses {
                    license {
                        name.set("Apache License 2.0")
                        url.set("https://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
                developers {
                    developer {
                        id.set("kdankert")
                        name.set("Kjell Dankert")
                        email.set("k.dankert@wtnet.de")
                    }
                }
                scm {
                    connection.set("scm:git:git://gitlab.com/cerocss/springkord.git")
                    developerConnection.set("scm:git:ssh://gitlab.com/cerocss/springkord.git")
                    url.set("https://gitlab.com/cerocss/springkord")
                }
            }
        }
        repositories {
            maven {
                url = uri("https://gitlab.com/api/v4/projects/40394207/packages/maven")
                name = "Gitlab"
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = System.getenv("CI_JOB_TOKEN")
                }
                authentication {
                    create<HttpHeaderAuthentication>("header")
                }
            }
        }
    }
}
