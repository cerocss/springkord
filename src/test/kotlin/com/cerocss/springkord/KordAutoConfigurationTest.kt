package com.cerocss.springkord

import dev.kord.core.Kord
import dev.kord.core.builder.kord.KordBuilder
import dev.kord.gateway.builder.Shards
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkConstructor
import kotlinx.coroutines.DelicateCoroutinesApi
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class KordAutoConfigurationTest {

    private val kordAutoConfiguration = KordAutoConfiguration()

    @Test
    @OptIn(DelicateCoroutinesApi::class)
    fun initKord() {
        // given
        val kord = mockk<Kord>() {
            every { resources } returns mockk() {
                every { token } returns ""
                every { shards } returns Shards(0)
            }
            every { gateway } returns mockk() {
                coEvery { startWithConfig(any()) } returns Unit
            }
        }

        mockkConstructor(KordBuilder::class)
        coEvery { anyConstructed<KordBuilder>().build() } returns kord

        // when
        val discordClient = kordAutoConfiguration.discordClient()

        // then
        assertThat(discordClient).isEqualTo(kord)
    }

    @Test
    @OptIn(DelicateCoroutinesApi::class)
    fun initKordPrivileged() {
        // given
        kordAutoConfiguration.privileged = true

        val kord = mockk<Kord>() {
            every { resources } returns mockk() {
                every { token } returns ""
                every { shards } returns Shards(0)
            }
            every { gateway } returns mockk() {
                coEvery { startWithConfig(any()) } returns Unit
            }
        }

        mockkConstructor(KordBuilder::class)
        coEvery { anyConstructed<KordBuilder>().build() } returns kord

        // when
        val discordClient = kordAutoConfiguration.discordClient()

        // then
        assertThat(discordClient).isEqualTo(kord)
    }
}
