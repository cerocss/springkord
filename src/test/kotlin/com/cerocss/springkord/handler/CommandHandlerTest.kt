package com.cerocss.springkord.handler

import com.cerocss.springkord.commands.KordCommands
import com.cerocss.springkord.roles.*
import com.cerocss.springkord.utils.SlashCommand
import com.cerocss.springkord.utils.UserCommand
import dev.kord.common.entity.Snowflake
import dev.kord.core.entity.interaction.ApplicationCommandInteraction
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.system.CapturedOutput
import org.springframework.boot.test.system.OutputCaptureExtension


@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(OutputCaptureExtension::class)
class CommandHandlerTest {

    private val applicationCommandInteraction: ApplicationCommandInteraction = mockk() {
        every { data.guildId.value } returns Snowflake(0L)
        coEvery { user.asMember(any()) } returns mockk() {
            coEvery { roles } returns emptyFlow()
        }
    }

    private val permissionHandler = DefaultPermissionHandler();
    private val roleTranslator: IRoleTranslator = mockk() {
        every { translateToHighestRole(listOf()) } returns DefaultRoles.DEVELOPER
    }

    private val commands = KordCommands(mapOf("SlashCommand" to SlashCommand(), "UserCommand" to UserCommand()))

    private val commandHandler = CommandHandler(commands, permissionHandler, roleTranslator)

    @Test
    fun testInvalidCommand(output: CapturedOutput) = runTest {
        // given
        every { applicationCommandInteraction.invokedCommandName } returns "invalid"

        // when
        commandHandler.handle(applicationCommandInteraction)

        // then
        assertTrue { output.out.contains("CommandHandler - Received interaction not registered: invalid") }
    }

    @Test
    fun testInvalidGuildId() = runTest {
        // given
        every { applicationCommandInteraction.data.guildId.value } returns null
        every { applicationCommandInteraction.invokedCommandName } returns "SlashCommand"

        // when / then
        assertThrows(IllegalStateException::class.java) {
            runBlocking {
                commandHandler.handle(
                    applicationCommandInteraction
                )
            }
        }
    }

    @Test
    fun testHandlingCommand() = runTest {
        // given
        every { applicationCommandInteraction.invokedCommandName } returns "SlashCommand"

        // when / then
        assertThrows(NotImplementedError::class.java) {
            runBlocking {
                commandHandler.handle(
                    applicationCommandInteraction
                )
            }
        }
    }

    @Test
    fun testInvalidPermissions(output: CapturedOutput) = runTest {
        // given
        every { applicationCommandInteraction.invokedCommandName } returns "UserCommand"

        // when
        commandHandler.handle(applicationCommandInteraction)

        // then
        assertTrue { output.out.contains("CommandHandler - Insufficient permission to execute the command: UserCommand") }
    }
}
