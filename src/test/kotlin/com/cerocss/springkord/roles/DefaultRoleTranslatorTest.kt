package com.cerocss.springkord.roles

import dev.kord.common.entity.Permissions
import dev.kord.common.entity.Snowflake
import dev.kord.common.entity.optional.Optional
import dev.kord.core.Kord
import dev.kord.core.cache.data.RoleData
import dev.kord.core.entity.Role
import dev.kord.core.supplier.EntitySupplier
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class DefaultRoleTranslatorTest {

    private val roleTranslator = DefaultRoleTranslator()

    private val kord: Kord = mockk()
    private val defaultSupplier: EntitySupplier = mockk()

    @Test
    fun translateToHighestRole() {
        // given
        val defaultRoles = DefaultRoles.values()
        val list = List(defaultRoles.size) {
            Role(
                RoleData(
                    id = Snowflake(it.toLong()),
                    guildId = Snowflake(0L),
                    name = defaultRoles[it].name,
                    color = it,
                    hoisted = false,
                    icon = Optional.invoke(),
                    unicodeEmoji = Optional.invoke(),
                    position = it,
                    permissions = Permissions(),
                    managed = false,
                    mentionable = false
                ), kord, defaultSupplier
            )
        }

        // when
        val result = roleTranslator.translateToHighestRole(list)

        // then
        assertEquals(defaultRoles.asList(), result.getHierarchy())
        assertEquals(DefaultRoles.ADMIN.getValue(), result.getValue())
    }

    @Test
    fun translateToHighestRoleIfRolesAreEmpty() {
        // given
        val defaultRoles = DefaultRoles.values()
        val list = listOf<Role>()

        // when
        val result = roleTranslator.translateToHighestRole(list)

        // then
        assertEquals(defaultRoles.asList(), result.getHierarchy())
        assertEquals(DefaultRoles.GUEST.getValue(), result.getValue())
    }
}
