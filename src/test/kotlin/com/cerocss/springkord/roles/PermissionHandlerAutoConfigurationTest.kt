package com.cerocss.springkord.roles

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.autoconfigure.AutoConfigurations
import org.springframework.boot.test.context.runner.ApplicationContextRunner

class PermissionHandlerAutoConfigurationTest {

    private val contextRunner =
        ApplicationContextRunner().withConfiguration(AutoConfigurations.of(PermissionHandlerAutoConfiguration::class.java))

    @Test
    fun initAllBeans() {
        this.contextRunner.run {
            assertThat(it).hasSingleBean(IPermissionHandler::class.java)
            assertThat(it).hasSingleBean(IRoleTranslator::class.java)
        }
    }
}
