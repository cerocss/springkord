package com.cerocss.springkord.roles

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

internal class DefaultPermissionHandlerTest {

    private val permissionHandler = DefaultPermissionHandler()

    companion object {
        @JvmStatic
        fun hasPermissions(): Stream<Arguments> = Stream.of(
            Arguments.of(DefaultRoles.ADMIN, DefaultRoles.MOD, true),
            Arguments.of(DefaultRoles.MEMBER, DefaultRoles.OBSERVER, false),
            Arguments.of(DefaultRoles.DEVELOPER, DefaultRoles.DEVELOPER, true)
        )
    }

    @ParameterizedTest
    @MethodSource
    fun hasPermissions(role: IRole, requiredRole: IRole, expected: Boolean) {
        // when
        val result = permissionHandler.hasPermissions(role, requiredRole)

        // then
        assertEquals(expected, result)
    }
}
