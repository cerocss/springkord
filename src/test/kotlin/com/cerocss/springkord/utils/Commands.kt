package com.cerocss.springkord.utils

import com.cerocss.springkord.commands.KordCommand
import com.cerocss.springkord.commands.KordCommandInterface
import com.cerocss.springkord.commands.KordCommandOptions
import com.cerocss.springkord.commands.KordCommandType
import com.cerocss.springkord.roles.DefaultRoles
import dev.kord.core.entity.interaction.ApplicationCommandInteraction
import dev.kord.rest.builder.interaction.BooleanBuilder

@KordCommand
class SlashCommand : KordCommandInterface<ApplicationCommandInteraction> {

    override val options = KordCommandOptions(
        name = "SlashCommand",
        type = KordCommandType.Slash,
        description = "TestDescription",
        requiredRole = DefaultRoles.DEVELOPER,
        options = mutableListOf(
            BooleanBuilder("TestOption", "TestOptionDescription")
        )
    )

    override suspend fun handle(interaction: ApplicationCommandInteraction) = throw NotImplementedError()
}

@KordCommand
class MessageCommand : KordCommandInterface<ApplicationCommandInteraction> {
    override val options = KordCommandOptions(
        name = "MessageCommand",
        type = KordCommandType.Message,
        description = "TestDescription",
        requiredRole = DefaultRoles.GUEST
    )

    override suspend fun handle(interaction: ApplicationCommandInteraction) = throw NotImplementedError()
}

@KordCommand
class UserCommand : KordCommandInterface<ApplicationCommandInteraction> {
    override val options = KordCommandOptions(
        name = "UserCommand",
        type = KordCommandType.User,
        description = "TestDescription",
        requiredRole = DefaultRoles.ADMIN
    )

    override suspend fun handle(interaction: ApplicationCommandInteraction) = throw NotImplementedError()
}
