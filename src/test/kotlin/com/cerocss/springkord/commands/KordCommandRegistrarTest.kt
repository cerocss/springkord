package com.cerocss.springkord.commands

import com.cerocss.springkord.utils.MessageCommand
import com.cerocss.springkord.utils.SlashCommand
import com.cerocss.springkord.utils.UserCommand
import dev.kord.common.entity.ApplicationCommandType
import dev.kord.common.entity.DiscordApplicationCommand
import dev.kord.common.entity.Snowflake
import dev.kord.core.Kord
import dev.kord.core.entity.application.GlobalApplicationCommand
import dev.kord.rest.json.request.ApplicationCommandCreateRequest
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.context.event.ContextRefreshedEvent

@ExperimentalCoroutinesApi
internal class KordCommandRegistrarTest {

    private val kord: Kord = mockk()
    private val contextRefreshedEvent: ContextRefreshedEvent = mockk()

    @Test
    fun handleReady() = runTest {
        // given
        val slashCommand = SlashCommand()
        val messageCommand = MessageCommand()
        val userCommand = UserCommand()
        val commands = KordCommands(
            mapOf(
                "SlashCommand" to slashCommand,
                "MessageCommand" to messageCommand,
                "UserCommand" to userCommand
            )
        )

        val kordCommandRegistrar = spyk(KordCommandRegistrar(kord, commands), recordPrivateCalls = true)

        val slot = slot<List<ApplicationCommandCreateRequest>>()
        val returnValue = listOf(
            DiscordApplicationCommand(
                id = Snowflake(0L),
                applicationId = Snowflake(0L),
                name = "",
                description = "",
                version = Snowflake(0L),
                defaultMemberPermissions = null
            )
        )

        every { kord.rest } returns mockk {
            every { interaction } returns mockk {
                coEvery { createGlobalApplicationCommands(any(), capture(slot)) } returns returnValue
            }
        }

        every { kord.resources } returns mockk {
            every { applicationId } returns Snowflake(0L)
        }

        every { kord.getGlobalApplicationCommands() } returns emptyFlow()

        // when
        kordCommandRegistrar.handleReady(contextRefreshedEvent)

        // then
        assertEquals(3, slot.captured.size)

        assertThat(slot.captured).anyMatch {
            slashCommand.options.name == it.name && ApplicationCommandType.ChatInput == it.type && it.options.value?.any { applicationCommandOption -> applicationCommandOption.name == slashCommand.options.options.first().name } ?: false
        }

        assertThat(slot.captured).anyMatch {
            userCommand.options.name == it.name && ApplicationCommandType.User == it.type
        }

        assertThat(slot.captured).anyMatch {
            messageCommand.options.name == it.name && ApplicationCommandType.Message == it.type
        }
    }

    @Test
    fun handleReadyForEmptyCommandsWithOldCommands() = runTest {
        // given
        val commands = KordCommands(emptyMap())
        val existingCommand = mockk<GlobalApplicationCommand>()

        val kordCommandRegistrar = spyk(KordCommandRegistrar(kord, commands), recordPrivateCalls = true)

        val slot = slot<List<ApplicationCommandCreateRequest>>()

        every { kord.rest } returns mockk {
            every { interaction } returns mockk {
                coEvery { createGlobalApplicationCommands(any(), capture(slot)) } returns emptyList()
            }
        }

        every { kord.resources } returns mockk {
            every { applicationId } returns Snowflake(0L)
        }

        every { kord.getGlobalApplicationCommands() } returns flowOf(existingCommand)

        coEvery { existingCommand.delete() } returns Unit

        // when
        kordCommandRegistrar.handleReady(contextRefreshedEvent)

        // then
        assertEquals(0, slot.captured.size)

        coVerify(exactly = 1) { existingCommand.delete() }
    }
}
