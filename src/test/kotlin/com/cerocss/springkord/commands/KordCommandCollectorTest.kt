package com.cerocss.springkord.commands

import com.cerocss.springkord.utils.SlashCommand
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.context.ApplicationContext

@ExperimentalCoroutinesApi
internal class KordCommandCollectorTest {

    private val kordCommandCollector = KordCommandCollector()

    private val applicationContext: ApplicationContext = mockk()

    @Test
    fun kordCommands() = runTest {
        // given
        val testCommand = SlashCommand()
        every { applicationContext.getBeansWithAnnotation(KordCommand::class.java) } returns mapOf(
            "TestCommand" to testCommand
        )

        // when
        val result = kordCommandCollector.kordCommands(applicationContext)

        // then
        assertEquals(result.size, 1)

        val first = result.firstNotNullOf { it }

        assertEquals(testCommand.options.name, first.key)
        assertEquals(testCommand.options, first.value.options)
    }
}
