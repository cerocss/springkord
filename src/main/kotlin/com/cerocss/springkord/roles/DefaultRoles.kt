package com.cerocss.springkord.roles

enum class DefaultRoles : IRole {
    GUEST, MEMBER, OBSERVER, MOD, DEVELOPER, ADMIN;

    override fun getHierarchy(): List<IRole> {
        return values().toList()
    }

    override fun getValue(): Int {
        return this.ordinal * 10
    }
}
