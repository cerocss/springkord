package com.cerocss.springkord.roles

import dev.kord.core.entity.Role

class DefaultRoleTranslator : IRoleTranslator {
    override fun translateToHighestRole(roles: List<Role>): IRole {
        val sortedRoles = roles.sortedWith { o1, o2 -> o2.compareTo(o1) }

        val maxSortedRole: String = sortedRoles.firstOrNull { role ->
            DefaultRoles.values().map { it.name }.contains(role.name.uppercase())
        }?.name?.uppercase() ?: "GUEST"

        return DefaultRoles.valueOf(maxSortedRole)
    }
}
