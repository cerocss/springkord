package com.cerocss.springkord.roles

interface IRole {

    fun getHierarchy(): List<IRole>

    fun isAtLeast(requirement: IRole): Boolean {
        return this.getValue() >= requirement.getValue()
    }

    fun getValue(): Int
}
