package com.cerocss.springkord.roles

import dev.kord.core.entity.Role

interface IRoleTranslator {

    fun translateToHighestRole(roles: List<Role>): IRole
}
