package com.cerocss.springkord.roles

import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component

@Component
class PermissionHandlerAutoConfiguration {

    @Bean
    fun permissionHandler(): IPermissionHandler {
        return DefaultPermissionHandler()
    }

    @Bean
    fun roleTranslator(): IRoleTranslator {
        return DefaultRoleTranslator()
    }
}
