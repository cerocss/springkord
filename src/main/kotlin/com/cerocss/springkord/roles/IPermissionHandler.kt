package com.cerocss.springkord.roles

interface IPermissionHandler {

    fun hasPermissions(role: IRole, requiredRole: IRole): Boolean {
        return role.isAtLeast(requiredRole)
    }
}
