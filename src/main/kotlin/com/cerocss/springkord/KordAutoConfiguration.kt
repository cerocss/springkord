package com.cerocss.springkord

import dev.kord.core.Kord
import dev.kord.gateway.Intent
import dev.kord.gateway.Intents
import dev.kord.gateway.PrivilegedIntent
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan(basePackages = ["com.cerocss.springkord"])
@ConfigurationProperties(prefix = "kord")
@ConditionalOnProperty(name = ["kord.enabled"], havingValue = "true", matchIfMissing = true)
class KordAutoConfiguration {

    var token: String = ""
    var privileged: Boolean = false

    private val logger = LoggerFactory.getLogger(this::class.java)

    @Bean
    @DelicateCoroutinesApi
    @OptIn(PrivilegedIntent::class)
    fun discordClient(): Kord {
        logger.info("Initializing Kord")
        val kord = runBlocking { Kord(token) }
        logger.info("Initialized Kord")
        GlobalScope.launch {
            kord.login {
                intents = Intents.all
                intents -= Intent.MessageContent
                if (!privileged) {
                    intents -= Intents.privileged
                }
            }
        }
        return kord
    }
}
