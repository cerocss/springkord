package com.cerocss.springkord.handler

import com.cerocss.springkord.commands.KordCommands
import com.cerocss.springkord.roles.IPermissionHandler
import com.cerocss.springkord.roles.IRoleTranslator
import dev.kord.core.entity.interaction.ApplicationCommandInteraction
import kotlinx.coroutines.flow.toList
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class CommandHandler(
    val commands: KordCommands,
    val permissionHandler: IPermissionHandler,
    val roleTranslator: IRoleTranslator
) {
    private val logger = LoggerFactory.getLogger(this::class.java)

    suspend fun handle(interaction: ApplicationCommandInteraction) {
        logger.debug("Command Interaction: ${interaction.invokedCommandName}")
        val command = commands[interaction.invokedCommandName]
        if (command != null) {
            val guildId = interaction.data.guildId.value ?: throw IllegalStateException()
            val roles = interaction.user.asMember(guildId).roles.toList()
            val maxRole = roleTranslator.translateToHighestRole(roles)

            if (permissionHandler.hasPermissions(maxRole, command.options.requiredRole)) {
                command.handle(interaction)
            } else {
                logger.warn("Insufficient permission to execute the command: ${interaction.invokedCommandName}")
            }
        } else {
            logger.warn("Received interaction not registered: ${interaction.invokedCommandName}")
        }
    }
}
