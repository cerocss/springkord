package com.cerocss.springkord

import com.cerocss.springkord.handler.CommandHandler
import dev.kord.core.Kord
import dev.kord.core.event.interaction.ApplicationCommandInteractionCreateEvent
import dev.kord.core.on
import org.slf4j.LoggerFactory
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component

@Component
class KordEventListener(
    val kord: Kord,
    val commandHandler: CommandHandler
) : ApplicationRunner {

    private val logger = LoggerFactory.getLogger(this::class.java)

    override fun run(args: ApplicationArguments?) {
        logger.info("Adding Event Listeners")
        kord.on<ApplicationCommandInteractionCreateEvent> {
            commandHandler.handle(interaction)
        }
        logger.info("Added Event Listeners")
    }
}
