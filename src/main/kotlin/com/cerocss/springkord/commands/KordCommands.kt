package com.cerocss.springkord.commands

import dev.kord.core.entity.interaction.ApplicationCommandInteraction

data class KordCommands(private val map: Map<String, KordCommandInterface<ApplicationCommandInteraction>>) :
    Map<String, KordCommandInterface<ApplicationCommandInteraction>> by map
