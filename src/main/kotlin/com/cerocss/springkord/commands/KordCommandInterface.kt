package com.cerocss.springkord.commands

import dev.kord.core.entity.interaction.ApplicationCommandInteraction

interface KordCommandInterface<T : ApplicationCommandInteraction> {
    val options: KordCommandOptions

    suspend fun handle(interaction: T)
}
