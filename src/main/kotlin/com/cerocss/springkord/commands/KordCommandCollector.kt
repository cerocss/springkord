package com.cerocss.springkord.commands

import dev.kord.core.entity.interaction.ApplicationCommandInteraction
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class KordCommandCollector {

    private val logger = LoggerFactory.getLogger(this::class.java)

    @Bean
    @Suppress("UNCHECKED_CAST")
    fun kordCommands(context: ApplicationContext): KordCommands {
        logger.info("Collecting commands")
        val commandMap = context.getBeansWithAnnotation(KordCommand::class.java)
        val kordCommands = KordCommands(commandMap.entries.associate {
            val value = it.value as KordCommandInterface<ApplicationCommandInteraction>
            value.options.name to value
        })
        logger.info("Collected commands: ${kordCommands.values}")
        return kordCommands
    }
}
