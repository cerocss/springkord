package com.cerocss.springkord.commands

import com.cerocss.springkord.roles.DefaultRoles
import com.cerocss.springkord.roles.IRole
import dev.kord.rest.builder.interaction.OptionsBuilder

data class KordCommandOptions(
    val name: String,
    val type: KordCommandType,
    val requiredRole: IRole = DefaultRoles.GUEST,
    val description: String = "",
    val options: MutableList<OptionsBuilder> = mutableListOf()
)
