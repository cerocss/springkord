package com.cerocss.springkord.commands

import dev.kord.core.Kord
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

@Component
class KordCommandRegistrar(val kord: Kord, val commands: KordCommands) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    @EventListener
    fun handleReady(event: ContextRefreshedEvent) {
        val groupedCommands = commands.values.groupBy { it.options.type }
        runBlocking {
            logger.info("Initializing commands")

            kord.getGlobalApplicationCommands().collect { it.delete() }

            kord.createGlobalApplicationCommands {
                groupedCommands[KordCommandType.User]?.forEach {
                    user(it.options.name) {}
                }
                groupedCommands[KordCommandType.Message]?.forEach {
                    message(it.options.name) {}
                }
                groupedCommands[KordCommandType.Slash]?.forEach {
                    input(it.options.name, it.options.description) {
                        options = it.options.options
                    }
                }
            }
            logger.info("Initialized commands")
        }
    }
}
